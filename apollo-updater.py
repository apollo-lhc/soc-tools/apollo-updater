#!/usr/bin/env python3

import os
import sys
import re
import time
import argparse
import subprocess

# The extraction directories for each type of tarball
OUTPUT_DIRECTORIES = {
    'filesystem' : '/',
    'firmware'   : '/fw/SM',
}

# Allowed verbosity level range
VERBOSITY_RANGE = (0,2)
VERBOSITY_ALLOWED = list(range(VERBOSITY_RANGE[0], VERBOSITY_RANGE[1]+1))

# List of (non-fatal) error message patterns that we know we'll encounter, and want to skip
TAR_ERR_IGNORE = [
    '.*Cannot change ownership to uid \d+, gid \d+: Operation not permitted.*'
]

# Expect filesystem tarballs to be > 100 MB in size, 
# and the firmware tarballs to be < 100 MB. 
TARBALL_SIZE_LIMIT = 100 * int(1e6)

# Settings related to the spinner displayed during
# the execution of the tar command
SPINNER_SETTINGS = {
    'interval' : 1, # Update spinner display every x seconds
    'characters' : [
        '\\', '|', '/', '-' # List of characters to display on screen
    ],
}

def user_is_root():
    """Returns True if the user is root."""
    return os.geteuid() == 0


def python_version_is_correct():
    """Returns True if the Python version is >= 3.6"""
    return sys.version_info >= (3,6)


def check_file_size(args):
    """
    Check the file size for the firmware and/or filesystem tarballs given.
    Filesystem tarballs: Expected to be > TARBALL_SIZE_LIMIT
    Firmware tarballs:   Expected to be < TARBALL_SIZE_LIMIT

    If a tarball is found not to be within the size limit, an AssertionError will be raised.
    """
    if args.fw:
        assert os.path.getsize(args.fw) < TARBALL_SIZE_LIMIT, (
            f"Size of firmware tarball {args.fw} is > {TARBALL_SIZE_LIMIT/1e6:.0f} MB, please check!")
    
    if args.fs:
        assert os.path.getsize(args.fs) > TARBALL_SIZE_LIMIT, (
            f"Size of filesystem tarball {args.fs} is < {TARBALL_SIZE_LIMIT/1e6:.0f} MB, please check!")


def validate_cli_args(args):
    """Validate the command line arguments, throw an IOError if some comnmand line options are invalid."""
    # At least one of the --filesystem and --firmware arguments must be present
    if not args.fs and not args.fw:
        raise IOError('One of the --fs (file system tarball) and --fw (firmware tarball) options must be given.')

    # Now check whether the given paths are valid for firmware and filesystem
    if args.fw:
        if not os.path.exists(args.fw):
            raise IOError(f'Input path does not exist for firmware tarball: {args.fw}')

    if args.fs:
        if not os.path.exists(args.fs):
            raise IOError(f'Input path does not exist for filesystem tarball: {args.fs}')

    # Check the tarball file sizes
    try: 
        check_file_size(args)
    except AssertionError as e:
        raise IOError(str(e))


def parse_cli():
    """Parse command line arguments and make some checks on the arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--fs', default=None, help='Path to the filesystem tarball.')
    parser.add_argument('--fw', default=None, help='Path to the firmware tarball.')
    parser.add_argument('-v', '--verbosity', type=int, 
        choices=VERBOSITY_ALLOWED, default=1, 
        help=f'The verbosity level, 0: min, {VERBOSITY_ALLOWED[-1]}: max.'
    )
    args = parser.parse_args()

    return args


def get_list_of_tarballs(args):
    """
    From the command line arguments, return a list of dictionaries,
    each representing a single tarball to be untarred.

    Note that if both FW and FS tarballs are given, FW one will be processed first.
    """
    files = []
    if args.fw:
        fw_info = {
            'path'     : args.fw,
            'filetype' : 'firmware',
            'outdir'   : OUTPUT_DIRECTORIES['firmware'],
        }
        files.append(fw_info)

    if args.fs:
        fs_info = {
            'path'     : args.fs,
            'filetype' : 'filesystem',
            'outdir'   : OUTPUT_DIRECTORIES['filesystem'],
        }
        files.append(fs_info)
    
    return files


def extract(path, filetype, outdir, numeric_owner, verbosity=0):
    """
    Extract the tarball given by the variable `path`.
    
    The tarball will be untarred into the directory specified via `outdir`.
    `filetype` specifies whether the tarball contains firmware or a file system.

    `numeric_owner` argument will be passed down to the untarring command to 
    assign permissions.
    """
    def vprint(vlevel, msg, **kwargs):
        """
        Print function with verbosity option.
        -> Make it simpler to print messages of different verbosity.
        """
        if vlevel <= verbosity:
            print(msg, **kwargs)
    
    vprint(1, f'> Extracting input file     : {path}')
    vprint(1, f'> Output directory to save  : {outdir}')
    
    vprint(2, f'> Verbosity level is set to : {verbosity}')
    vprint(2, f'> File type to extract      : {filetype}')

    # Check if the output directory exists
    if not os.path.exists(outdir):
        raise RuntimeError(f'Output directory for {filetype} tarball does not exist: {outdir}')

    # Extract the tarball to the target directory
    cmd = ['tar', '-p', '-xf', path, '-C', outdir]
    if numeric_owner:
        cmd.insert(1, '--numeric-owner')

    vprint(1, f'> Extracting {path}')
    vprint(2, f'> {" ".join(cmd)}')

    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Run the process, display a spinning loading bar while doing that
    i_char = 0
    num_spinner_chars = len(SPINNER_SETTINGS['characters'])

    return_code = None

    #
    # While the process is not complete, proc.poll() will return None.
    # Keep iterating until the process is complete, and update the spinning
    # bar display every SPINNER_SETTINGS['interval'] seconds.
    #
    while True:
        return_code = proc.poll()
        # Process is completed, break out of the loop
        if return_code is not None:
            break

        char = SPINNER_SETTINGS['characters'][i_char % num_spinner_chars]
        vprint(1, f'> {char}', end='\r')
        i_char += 1

        # Sleep for a while
        time.sleep(SPINNER_SETTINGS['interval'])
        
    vprint(1, '> Extraction complete!')
    vprint(2, f'> tar return code: {return_code}.')

    # Check if the process completed without any fatal errors
    # Check out this docs page for more info on tar return codes:
    # https://man7.org/linux/man-pages/man1/tar.1.html#RETURN_VALUE
    if return_code != 0:
        # Check if this is an error we want to skip
        errmsg = proc.stderr.read().decode('utf-8')
        skip = False
        for item in TAR_ERR_IGNORE:
            if re.match(item, errmsg):
                skip = True
                break
        
        # If this is a new error, throw a RuntimeError
        # This RuntimeError will be caught by the main() function.
        if not skip:
            raise RuntimeError(errmsg)

    # If return code is 0 -> the process terminated without fatal errors
    vprint(1, '> Done!')


def main():
    # Parse command line arguments
    args = parse_cli()

    # Validate command line arguments
    try:
        validate_cli_args(args)
    except IOError as e:
        print(f'Error raised during command line validation:\n> {str(e)}')
        print('> Exiting.')
        return 1

    # Check: 
    # 1. If the user has root priviliges
    # 2. Is the python version >= 3.6 (required for f-strings)?
    if not user_is_root():
        print('You need root access to run the script, exiting.')
        return 2

    if not python_version_is_correct():
        print('You need python>=3.6 to run this script, exiting.')
        return 3

    # Get the list of tarballs according to the CLI arguments
    files = get_list_of_tarballs(args)
    
    # Loop over the files and do the extraction
    for file_info in files:
        
        # File info
        path = file_info['path']
        filetype = file_info['filetype']
        outdir = file_info['outdir']

        # Permission-related for untarring
        numeric_owner = filetype == 'filesystem'

        # Extract (try to) this tarball
        try:
            extract(
                path, 
                filetype,
                outdir,
                numeric_owner,
                args.verbosity
            )
        
        # Catch any fatal exception during the underyling extract call
        except RuntimeError as e:
            print(f'RuntimeError occured during extract() call:\n> {str(e)}')
            print('> Exiting.')
            return 4

    return 0

if __name__ == '__main__':
    main()
