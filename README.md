# Apollo FW Updater

This repository contains an `apollo-updater.py` script, that can be used to update the firmware for the Apollo Service Module (SM). If the tarball file including the firmware build and/or the filesystem build is present, this script can be used to untar it in the correct place in the filesystem of Zynq chip, on the Apollo SM.

More details on how to use the script is provided below.

## Pre-requisites

The user should have SSH access to an Apollo SM, on which the `apollo-updater.py` script can be executed. All the libraries this script uses come with the `Python3` installation, so no additional installations are required. The following however, will be required:

* The user **must** be logged in as the `root` user to the SM.
* A Python version of `>=3.6` is required. In Apollo SMs, a `python3.6` installation should be already there.

If these are satisfied, just clone the `main` branch of this repository to the Apollo SM:

```bash
git clone https://gitlab.com/apollo-lhc/soc-tools/apollo-updater.git
cd apollo-updater
```

## Running The Script

Once the tarball is secure copied into the target Apollo SM, running this script is easy! The user can follow the following steps:

* Secure copy the tarball (containing filesystem and/or kernel build) to the SM

```bash
scp /path/to/tarball.tar.gz root@apolloXYZ:/path/to/target
```

**Note:** The updater script will untar the filesystem and FW tarballs to their designated spot:

* FW tarballs: `/fw/SM`
* Filesystem tarballs: `/`

To keep things simple, it is recommended to copy the tarball to those same directories.

* Run the `apollo-updater.py` script to untar the tarball to the designated location, as follows:

```bash
./apollo-updater.py --fw /path/to/fw_tarball.tar.gz --fs /path/to/fs_tarball.tar.gz
```

A few notes:
* The user can provide only one of the `--fw` and `--fs` options as well, e.g. `./apollo-updater.py --fw /path/to/fw.tar.gz` is a valid call.
* **At least one** of the `--fw` or `--fs` options must be provided.
* If both `--fw` and `--fs` options are present, the firmware will be untarred first.
* The script also supports a `-v` option for verbosity (defaults to `1`), the allowed verbosity values are `0`, `1` and `2`.

The user can also execute the script with `--help` option to get a list of command line arguments.

